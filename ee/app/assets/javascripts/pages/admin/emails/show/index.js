import { initAdminEmailsForm } from 'ee/admin/emails';
import AdminEmailSelect from './admin_email_select';

AdminEmailSelect();
initAdminEmailsForm();
